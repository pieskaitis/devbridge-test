# Devbridge test task

### Requirements
This environment requires [Node.js](https://nodejs.org/) to run. You can either use [NPM](https://www.npmjs.com/) or [Yarn](https://www.yarnpkg.com).

### Installation
Clone the repository:
```sh
$ git clone git@bitbucket.org:pieskaitis/devbridge-test.git
```

Change directory to the cloned one:
```sh
$ cd devbridge-test
```

Install the dependencies:

*- Via NPM*
```sh
$ npm i
```

*- Via Yarn*
```sh
$ yarn
```

### Development
Start the dev-server with hot-reloading:

*- Via NPM*
```sh
$ npm run dev
```

*- Via Yarn*
```sh
$ yarn dev
```

### Production
Build for production:

*- Via NPM*
```sh
$ npm run build
```

*- Via Yarn*
```sh
$ yarn build
```
All the files will be compiled to `dist` directory.

----
### Author
**Mantas Pieškaitis**
