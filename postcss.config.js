const devMode = process.env.NODE_ENV !== 'production';
const CSSnano = devMode ? null : require('cssnano')

module.exports = {
  plugins: [
    require('autoprefixer'),
    CSSnano
  ]
}
