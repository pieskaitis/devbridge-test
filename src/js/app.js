import $ from 'jquery';
import 'jquery-validation';

$(function() {
  var mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;

  var menuBurger = $('#nav-burger');
  var navHeader = $('nav.header');
  var navMenuItems = $('nav.header ul.nav:not(.top) li');

  $('#contact').validate({
    rules: {
      firstName: {
        required: true,
        minlength: 2
      },
      lastName: {
        required: true,
        minlength: 2
      },
      message: {
        required: true,
        minlength: 10
      }
    },
    submitHandler: function(form) {
      // ajax requestas
      form.reset();
      alert('Success!');
    }
  })

  menuBurger.click(function(e) {
    e.preventDefault();

    navHeader.slideToggle('fast');
  })

  if (mobile) {
    navMenuItems.click(function(e) {
      e.preventDefault();

      var currElem = $(this).find('.dropdown');

      currElem.toggleClass('open');
      $(this).parent().find('.dropdown').not(currElem).removeClass('open');
    })
  } else {
    navMenuItems.click(function(e) {
      e.preventDefault();
    })

    navMenuItems.hover(function() {
      $(this).find('.dropdown').addClass('open');
    }, function() {
      $(this).find('.dropdown').removeClass('open');
    })
  }
});
